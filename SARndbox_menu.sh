#!/bin/bash

#config
configdir=~/.config/Vrui-4.2
videoprojname="DP1"
videoprojresol="800 600"
vruiVerbose="" #"-vruiVerbose"
edit="/usr/bin/pluma"


if test ! -s $configdir
	then
	mkdir $configdir
fi
if test ! -s $configdir/Applications
	then
	mkdir $configdir/Applications
fi

if test ! -s $configdir/Applications/CalibrateProjector.cfg
then
echo "
section Vrui
    section Desktop
        section Window
            windowFullscreen true
            outputName $videoprojname
        endsection
        
        section Tools
            section DefaultTools
                section CalibrationTool
                    toolClass CaptureTool
                    bindings ((Mouse, Mouse4, Mouse5))
                endsection
            endsection
        endsection
    endsection
endsection" > $configdir/Applications/CalibrateProjector.cfg
fi

if test ! -s $configdir/Applications/RawKinectViewer.cfg
then
echo "
section Vrui
    section Desktop
        section Tools
            section DefaultTools
			 section Plan
				 toolClass PlaneTool
				 bindings ((Mouse, Num1))
			 endsection
			 section Mesure
				 toolClass DepthMeasurementTool
				 bindings ((Mouse, Num2))
			 endsection
            endsection
        endsection
    endsection
endsection" > $configdir/Applications/RawKinectViewer.cfg
fi

if test ! -s $configdir/Applications/SARndbox.cfg
then
echo "
section Vrui
    section Desktop
        section MouseAdapter
            mouseIdleTimeout 5.0
        endsection
        
        section Window
            windowFullscreen true
            outputName $videoprojname
        endsection
        
        section Tools
            section DefaultTools
                section WaterTool
                    toolClass GlobalWaterTool
                    bindings ((Mouse, 1, 2))
                    LocalWaterTool
                    bindings ((Mouse, 4, 5))
                endsection
            endsection
        endsection
    endsection
endsection
" > $configdir/Applications/SARndbox.cfg
fi

verifkinect()
{
if [ $(lsusb|grep "ID 045e:" -c) -lt 3 ]
then
zenity --warning --title "Kinect non trouvé" \
--text="Vérifier que le Kinect est bien branché à l'addaptateur 
et que celui-ci est bien branché sur une prise USB de l'ordinateur et sur le secteur."
$0
fi
}

verifproj()
{
if [ $(ls /tmp/.X11-unix/|wc -l) -lt 2 ]
then
zenity --warning --title "Vidéoprojecteur non trouvé" \
--text="Vérifier que le vidéoprojecteur est bien branché à l'ordinateur, 
sur le secteur et qu'il est bien allumé."
$0
fi
}

choisir()
{

while [ ! "$CHOIX_ACTION" ]
do
CHOIX_ACTION=$(zenity --list --width "600" --height "300" \
		--title "Bac à sable interactif" \
		--text "Lancement des différents sous programmes.\n
" \
		--radiolist \
		--column "Choix" --column "Description" \
		False "Calibration Kinect"\
		False "Calibration Projecteur"\
		True "Lancer le bac à sable"\
		False "Quitter"\
)

case $CHOIX_ACTION in
    "Calibration Kinect")
        zenity --text-info --filename="/home/bacasable/src/RawKinectViewer.txt" --title "Calibration Kinect" &
        $edit ~/src/SARndbox-2.3/etc/SARndbox-2.3/BoxLayout.txt &
		RawKinectViewer -compress 0 $vruiVerbose
		$0
        ;;
    "Calibration Projecteur")
        zenity --text-info --filename="/home/bacasable/src/CalibrateProjector.txt" --title "Calibration du projecteur" &
		~/src/SARndbox-2.3/bin/CalibrateProjector -s $videoprojresol $vruiVerbose
		$0
        ;;
    "Lancer le bac à sable")
		~/src/SARndbox-2.3/bin/SARndbox -uhm -fpv $vruiVerbose
		$0
		;;
	"Quitter")
	exit 1;
        ;;
esac
done
}

verifkinect
#lsverifproj
choisir
